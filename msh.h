/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   msh.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 09:39:31 by arangari          #+#    #+#             */
/*   Updated: 2017/12/21 14:15:37 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MSH_H
# define MSH_H

# define MAXLINE 80
# define MAXARGS 20

# include <stddef.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <strings.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <sys/types.h>
# include "libft/libft.h"

char		**g_envv;

int			exec_bin(char **args);
void		print_env(void);
int			envv_len(char **envv);
int			my_echo(char **args);
int			my_unsetenv(char **args);
int			my_setenv(char **args);
char		*find_envv(char *var);
int			find_position(char *var);
int			my_cd(char **args);
void		set_env(char *name, char *value);

#endif
