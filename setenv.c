/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/11 16:50:31 by arangari          #+#    #+#             */
/*   Updated: 2017/12/21 14:35:03 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "msh.h"

char				*find_envv(char *var)
{
	int				i;
	char			*temp;

	i = 0;
	while (g_envv[i])
	{
		temp = ft_strjoin(var, "=");
		if (ft_startwith(g_envv[i], temp))
		{
			free(temp);
			return (ft_strchr(g_envv[i], '=') + 1);
		}
		free(temp);
		i++;
	}
	return (NULL);
}

int					find_position(char *var)
{
	int				i;
	char			*temp;

	i = 0;
	while (g_envv[i])
	{
		temp = ft_strjoin(var, "=");
		if (ft_startwith(g_envv[i], temp))
		{
			free(temp);
			return (i);
		}
		free(temp);
		i++;
	}
	return (i);
}

static char			**realloc_envv(int new_size)
{
	char			**new_envv;
	int				i;

	i = -1;
	new_envv = (char**)malloc(sizeof(char*) * (new_size + 1));
	while (g_envv[++i] && i < new_size)
	{
		new_envv[i] = ft_strdup(g_envv[i]);
		free(g_envv[i]);
	}
	free(g_envv);
	return (new_envv);
}

void				set_env(char *name, char *value)
{
	int				pos;
	char			*temp;

	pos = find_position(name);
	temp = ft_strjoin("=", value);
	if (g_envv[pos])
	{
		free(g_envv[pos]);
		if (value)
			g_envv[pos] = ft_strjoin(name, temp);
	}
	else
	{
		g_envv = realloc_envv(pos + 1);
		if (value)
			g_envv[pos] = ft_strjoin(name, temp);
		g_envv[pos + 1] = NULL;
	}
	free(temp);
}

int					my_setenv(char **args)
{
	if (!args[0] || (args[0] && !args[1]))
	{
		ft_putendl("setenv: \033[31mToo few arguments.\033[0m");
		return (1);
	}
	if (args[0] && args[1])
	{
		if (args[2])
		{
			ft_putendl("setenv: \033[31mToo many arguments.\033[0m");
			return (1);
		}
		set_env(args[0], args[1]);
	}
	return (1);
}
