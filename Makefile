# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: arangari <arangari@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/12 10:23:46 by arangari          #+#    #+#              #
#    Updated: 2018/01/03 11:35:33 by arangari         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell

FLAGS = gcc -g -Wall -Werror -Wextra

SRC  = cd_imp.c echo_imp.c exec_cmd.c setenv.c my_environ.c main.c

OBJ = $(SRC:%.c=%.o)

all:$(NAME)

$(NAME): $(OBJ)
	@make -C libft/
	@$(FLAGS) -c $(SRC)
	@$(FLAGS) -o $(NAME) $(SRC) -L libft/ -lft

clean:
	@make clean -C libft/
	@rm -rf $(OBJ)

fclean: clean
	@make fclean -C libft/
	@rm -rf $(NAME)

re:fclean all

.PHONY:all clean fclean re