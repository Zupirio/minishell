/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_array.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 11:45:28 by arangari          #+#    #+#             */
/*   Updated: 2017/12/21 14:19:19 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_free_array(char **str)
{
	int		i;

	if (!str)
		return ;
	i = -1;
	while (str[++i])
	{
		free(str[i]);
	}
	free(str);
	str = NULL;
}
