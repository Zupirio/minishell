/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: arangari <arangari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/12 16:53:43 by arangari          #+#    #+#             */
/*   Updated: 2017/12/21 14:36:21 by arangari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./get_next_line.h"
#include "./libft.h"

static int				ft_cpy_contents(int const fd, char **stack)
{
	char				*buff;
	char				*temp;
	int					value;

	if (!(buff = (char *)malloc(sizeof(*buff) * (BUFF_SIZE + 1))))
		return (-1);
	value = read(fd, buff, BUFF_SIZE);
	if (value > 0)
	{
		buff[value] = '\0';
		temp = ft_strjoin(*stack, buff);
		free(*stack);
		*stack = temp;
	}
	free(buff);
	return (value);
}

int						memleak(char **line, char **stack, char **line_feed)
{
	char				*tmp;

	*line = ft_strsub(*stack, 0, ft_strlen(*stack) - ft_strlen(*line_feed));
	tmp = *stack;
	*stack = ft_strdup(*line_feed + 1);
	free(tmp);
	return (1);
}

int						get_next_line(const int fd, char **line)
{
	static char			*stack = NULL;
	char				*line_feed;
	int					value;
	char				*tmp;

	if ((!stack && (stack = (char *)malloc(sizeof(*stack))) == NULL) || !line
		|| fd < 0 || BUFF_SIZE < 0)
		return (-1);
	line_feed = ft_strchr(stack, '\n');
	while (line_feed == '\0')
	{
		value = ft_cpy_contents(fd, &stack);
		if (value == 0)
		{
			if (ft_strlen(stack) == 0 || !(tmp = stack))
				return (0);
			stack = ft_strjoin(stack, "\n");
			free(tmp);
		}
		if (value < 0 || !(line_feed = ft_strchr(stack, '\n')))
			return (-1);
	}
	return (memleak(line, &stack, &line_feed));
}
